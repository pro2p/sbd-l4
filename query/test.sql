SELECT * from film;
SELECT * from category;
SELECT * from customer;
SELECT * from film_actor;
SELECT * from inventory;
SELECT * from rental;

SELECT * from inventory;
SELECT COUNT(*) from inventory;

--rental--
SELECT * from rental;
SELECT count(*) from rental;

DELETE from rental;



INSERT INTO rental (RENTAL_DATE, INVENTORY_ID, customer_id, return_date) 
values
('2019-05-05 08:16',1,3,'2019-05-01'),
('2019-05-05 08:17',1,3,'2019-05-09'),
('2019-05-05 08:18',1,6,'2019-05-09'),
('2019-05-05 08:19',1,4,'2019-05-09')

CREATE UNIQUE INDEX idx_unq_rental_rental_date_inventory_id_customer_id 
ON public.rental 
USING btree (rental_date, inventory_id, customer_id);