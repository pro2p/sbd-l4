select distinct film.film_id, title, release_year, category.name as category
	,(SELECT COUNT(*)
     FROM film_actor AS a
     WHERE film.film_id = a.film_id
 	) as "no_cast"
	,(SELECT COUNT(*)
     FROM inventory AS i
     WHERE film.film_id = i.film_id
 	) as "inventory"
	,COUNT(rental_id) as "rentings"

FROM film, category, film_category ,inventory 

INNER JOIN rental ON rental.inventory_id = inventory.inventory_id

WHERE category.category_id= film_category.category_id
	and film_category.film_id = film.film_id
	 	AND film.film_id = inventory.film_id

GROUP BY film.film_id, title, release_year, category.name

ORDER BY film.title ASC
;