pipeline = [
            {"$unwind": "$rentals"},
            {"$project":
                {
                    "film_id": "$rentals.inventory.film.film_id",
                    "title": "$rentals.inventory.film.title",
                    "release_year": "$rentals.inventory.film.release_year",
                    "category": "$rentals.inventory.film.categories.name",
                    "no_actors": {"$size": "$rentals.inventory.film.actors"},
                    "_id": 0,
                },
            },
             {
             "$group" : {
                "_id" : { "film_id": "$film_id", "title": "$title", "release_year":"$release_year", "category":"$category", "no_actors":"$no_actors"},
                "count": { "$sum": 1 },
              },
            },
            {"$project":
                {
                    "film_id": "$_id.film_id",
                    "title": "$_id.title",
                    "release_year": "$_id.release_year",
                    "category": "$_id.category",
                    "no_actors": "$_id.no_actors",
                    "rentals": "$count",
                    "_id": 0,
                },
            },   
            { "$sort" : { "title" : 1 } }
        ]
db.getCollection('Customers').aggregate(pipeline).toArray()
//db.getCollection('Customers').aggregate(pipeline).toArray().length
