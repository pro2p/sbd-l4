--film, category, actor, inventory, rental, //customer

select film.film_id, title, film.release_year, category.name as category
	,(SELECT COUNT(*)
     FROM film_actor AS a
     WHERE film.film_id = a.film_id
 	) as "no_cast"
	,(SELECT COUNT(*)
     FROM inventory AS i
     WHERE film.film_id = i.film_id
 	) as "inventory"
	,COUNT(rental_id) as "rentings"

FROM film, category, film_category ,inventory --,rental

INNER JOIN rental ON rental.inventory_id = inventory.inventory_id
--INNER JOIN inventory ON inventory.film_id = film.film_id 
--JOIN customer ON customer.customer_id = rental.customer_id

WHERE category.category_id= film_category.category_id
	and film_category.film_id = film.film_id
	 	AND film.film_id = inventory.film_id
		--AND inventory.inventory_id = rental.inventory_id
	---AND customer.create_date> '1900-01-01'

GROUP BY film.film_id, title, release_year, category.name

--HAVING no_cast>10
ORDER BY film.title ASC
--ORDER BY rentings DESC
--ORDER BY rental_rate ASC
;