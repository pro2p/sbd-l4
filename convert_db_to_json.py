#!/usr/bin/env python3
import pandas as pd
import os
import json
import numpy 

def default(o):
    if isinstance(o, numpy.int64) or isinstance(o, numpy.int32):
         return int(o)  
    raise TypeError




headers = {
    "actor": ["actor_id", "first_name", "last_name"],
    "category": ["category_id", "name"],
    "customer": ["customer_id", "first_name", "last_name", "email", "active"],
    "film": ["film_id", "title", "rental_rate", "length", "release_year"],
    "film_actor": ["actor_id", "film_id"],
    "film_category": ["film_id", "category_id"],
    "inventory": ["inventory_id", "film_id"],
    "rental": ["rental_date", "inventory_id", "customer_id", "return_date"]
}
tables = {}
for table_name, header in headers.items():
    if table_name != "rental":
        path = os.path.join("dat", table_name + ".dat")
        tables[table_name] = pd.read_csv(path, sep="\t", names=header, skipfooter=3, engine='python').to_dict('records')
    else:
        path = os.path.join("dat", table_name + "_250k_seq.csv")
        tables[table_name] = pd.read_csv(path, sep="\t", names=header).to_dict('records')

# customers_size = len(tables['customer'])

# for ind, customer in enumerate(tables['customer']):
#     customer['rentals'] = [r for r in tables['rental'] if r['customer_id'] == customer['customer_id']]
#     for r in customer['rentals']:
#         r['inventory'] = [i for i in tables['inventory'] if i['inventory_id'] == r['inventory_id']][0]
#         r['inventory']['film'] = [f for f in tables['film'] if f['film_id'] == r['inventory']['film_id']][0]

#         r['inventory']['film']['actors'] = []
#         for fa in tables['film_actor']:
#             if fa['film_id'] == r['inventory']['film']['film_id']:
#                 actor = [a for a in tables['actor'] if a['actor_id'] == fa['actor_id']][0]
#                 r['inventory']['film']['actors'].append(actor)

#         r['inventory']['film']['categories'] = []
#         for fc in tables['film_category']:
#             if fc['film_id'] == r['inventory']['film']['film_id']:
#                 category = [c for c in tables['category'] if c['category_id'] == fc['category_id']][0]
#                 r['inventory']['film']['categories'].append(category)
#     # print(customer)
#     print(round((ind/customers_size)*100, 2), "%", end = '\r')

for customer in tables['customer']:
    customer['rentals'] = []
rentals_size = len(tables['rental'])

for ind, rental in enumerate(tables['rental']):
    rental['inventory'] = [i for i in tables['inventory'] if i['inventory_id'] == rental['inventory_id']][0]
    rental['inventory']['film'] = [f for f in tables['film'] if f['film_id'] == rental['inventory']['film_id']][0]

    rental['inventory']['film']['actors'] = []
    for fa in tables['film_actor']:
        if fa['film_id'] == rental['inventory']['film']['film_id']:
            actor = [a for a in tables['actor'] if a['actor_id'] == fa['actor_id']][0]
            rental['inventory']['film']['actors'].append(actor)

    rental['inventory']['film']['categories'] = []
    for fc in tables['film_category']:
        if fc['film_id'] == rental['inventory']['film']['film_id']:
            category = [c for c in tables['category'] if c['category_id'] == fc['category_id']][0]
            rental['inventory']['film']['categories'].append(category)
    cust_index = next((index for (index, c) in enumerate(tables['customer']) if c["customer_id"] == rental["customer_id"]), None)
    tables['customer'][cust_index]['rentals'].append(rental)
    print(round((ind/rentals_size)*100, 2), "%", end = '\r')


with open("customers_250.json", 'w+') as f:
    # json.dump(tables['customer'], f, default=default, indent=2)
    json.dump(tables['customer'], f, default=default)
# for i in tables['customer']:
#     print(i)